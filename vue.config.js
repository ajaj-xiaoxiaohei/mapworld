module.exports = {
    transpileDependencies: true,
    chainWebpack: config => {
        const oneOfsMap = config.module.rule('less').oneOfs.store
        oneOfsMap.forEach(item => {
            item
                .use('sass-resources-loader')
                .loader('sass-resources-loader')
                .options({
                    resources: './src/style/global.less' // 全局less文件的地址
                })
                .end()
        })
    }
}